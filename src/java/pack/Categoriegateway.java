package pack;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author user
 */


public class Categoriegateway 
{
    
     private Connection conn;
    private PreparedStatement prodcat,prod;
    private ResultSet result;
    
    private  String url;
    private  String user;
    private  String pw;

   
    public Categoriegateway(String url,String user,String pw) throws SQLException
    {
       try{
           Class.forName("com.mysql.jdbc.Driver");
              this.url = url;
              this.user = user;
              this.pw = pw;
        try
        {
         
           this.conn = DriverManager.getConnection(this.url, this.user, this.pw);
             prodcat=conn.prepareStatement("select * from produit where id_cat=?;");
          
             prod=conn.prepareStatement("select * from produit where  nom=? ;");

             
        } 
        catch (SQLException ex)
        {
            Logger.getLogger(Categoriegateway.class.getName()).log(Level.SEVERE, null, ex);
        } 
       }
        catch (ClassNotFoundException ex) {
             Logger.getLogger(Categoriegateway.class.getName()).log(Level.SEVERE, null, ex);
         }
    }

    public ArrayList<Produits > getprodcat(Integer cat)
    {
        ArrayList<Produits> listOfleg = new ArrayList<Produits>();
        
        try    {
            
            this.prodcat.setInt(1, cat);
           
            result = this.prodcat.executeQuery();
            
            while(result.next())
            {
            
                
                listOfleg.add(new Produits(result.getInt("id_prod"),result.getString("nom"),
                                  result.getDouble("prix"),result.getString("desc"),result.getString("img")));
            }
            
           
        }
        catch (SQLException ex)
        {
            Logger.getLogger(Categoriegateway.class.getName()).log(Level.SEVERE, null, ex);
        }
        
         return listOfleg;
    }
    
    
    public Produits getprod (String nom)
    {
         Produits PrRec = new Produits();
         
         try {
           //  PrRec = new Produits();
             this.prod.setString(1, nom);
             result=this.prod.executeQuery();
             while(result.next())
            {
            
                
                PrRec=new Produits(result.getInt("id_prod"),result.getString("nom"),
                                  result.getDouble("prix"),result.getString("desc"),result.getString("img"));
            }
             
           
             
         } catch (SQLException ex) {
            System.out.println(ex.getMessage());
         }
    
        return PrRec;
    
    }
    
    
 
}
