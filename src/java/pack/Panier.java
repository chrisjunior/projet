/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pack;

import java.util.ArrayList;

/**
 *
 * @author user
 */
public class Panier {

    public int id;
    public ArrayList<Produits> ListProduit;

    public Panier() {
        this.id = 0;
        this.ListProduit=null;

    }

    public Panier(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public Panier(ArrayList<Produits> ListProd) {
        this.ListProduit = ListProd;
    }

}
