package pack;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author user
 */


public class Produits {
    private int id;
    private String nom;
    private double prix;
    private String image;
    private String desc;
    private int quantite ;
    
     public Produits()
    {
        this.nom="n/a";
        this.prix=0.0; 
        this.desc="n/a";
         this.image="n/a";
    }
     
      public Produits(int id,String nom, double prix, String desc,String img)
    {
        this.id=id; 
        this.nom=nom;
        this.prix=prix;
        this.image=img;
        this.desc=desc;
       
    }
         public Produits(int id,String nom, double prix, String desc,String img,int qte)
    {
        this.id=id; 
        this.nom=nom;
        this.prix=prix;
        this.image=img;
        this.desc=desc;
        this.quantite=qte;
       
    }
     public String getnom()
    {
        return this.nom;
    }
    public double getprix()
    {
        return this.prix;
    }
     public String getdesc()
    {
        return this.desc;
    }
       public int getQte()
    {
        return this.quantite;
    }
    
    public void setnom(String nom)
    {
         this.nom = nom;
    }
    public void setprix(double prix)
    {
         this.prix = prix;
    }
    public void setdesc(String desc)
    {
         this.desc = desc;
    }
      public void setQte(int qte)
    {
         this.quantite=qte;
    }
      
    public int getid()
    {
         return this.id;
    }
     public String getimg()
    {
         return this.image;
    }
     

     
     
}
