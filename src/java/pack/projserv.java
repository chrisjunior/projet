package pack;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pack.Categoriegateway;
import pack.Produits;
import pack.categorie;
import pack.servtest;

/**
 *
 * @author user
 */
@WebServlet(urlPatterns = {"/serv"})
public class projserv extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet projserv</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet projserv at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        /*  processRequest(request, response);
        PrintWriter out = response.getWriter();*/
        
        
       try {
           String url = "jdbc:mysql://localhost:3306/projet?zeroDateTimeBehavior=convertToNull";
        String user = "root";
        String pw = "";

            Categoriegateway c = new Categoriegateway(url, user, pw);
       
            String cat = request.getParameter("chris");
            if(cat!= null){
                ArrayList<Produits> prod = c.getprodcat(Integer.parseInt(cat));
                 categorie unecat = new categorie(prod);

            request.setAttribute("coco", unecat);
            RequestDispatcher dispatch = request.getRequestDispatcher("/produit.jsp");
            dispatch.forward(request, response);
            }
            else{
                processRequest(request, response);
        PrintWriter out = response.getWriter();
            }

            

         
            

            

      } catch (SQLException ex) {
            Logger.getLogger(servtest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
         
           String url = "jdbc:mysql://localhost:3306/projet?zeroDateTimeBehavior=convertToNull";
         String user = "root";
            String pw = "";
        try {
         Categoriegateway cr = new Categoriegateway(url, user, pw);            

           String nom = request.getParameter("name");

            Produits prd = cr.getprod(nom);
           response.getWriter().write(prd.getid() + "*" + prd.getdesc() + "*" + prd.getnom() + "*" + prd.getimg() +"*" + prd.getprix());
           
        } catch (SQLException ex) {
            Logger.getLogger(projserv.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
