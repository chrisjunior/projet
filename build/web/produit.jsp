<%-- 
    Document   : listProd
    Created on : 2019-12-20, 18:18:16
    Author     : user
--%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8" import="pack.Produits,pack.categorie"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<style>
    .rounded-circle{
        height:200px;
    }
    
</style>
        <title>JSP Page</title>
    </head>
    <body>
         <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
  <a class="navbar-brand" href="./">Acceuil</a>
  <a class="navbar-brand" href="./panier.jsp">Panier d'Achat</a>
  
  ...
</nav>
        <br><br>
        <img src="logos-1.png" class="rounded-circle" alt="Cinque Terre">
    <center> <h2>Christian au naturel</h2></center>
    <br><br><br>
        
        <div style="height:500px;">

            <div style="float: left;width: 40%;height: 500px">
                <center>
                    <form method="post">
                        <br/><br/>
                        <table>

                            <%
                                categorie c = (categorie) request.getAttribute("coco");
                            %>

                            <% for (Produits pr : c.ListProduit) {
                            
                            %>  

                            <tr> 
                                <td> 
                                    <div class="w3-card-4" style="width:200px">
                                        <div class="w3-container w3-center">
                                            <input type="button" id="<%=pr.getid()%>"  class="koko" value="<%=pr.getnom()%> " name="<%=pr.getnom()%>" size="200px" />
                                             <img src="<%=pr.getimg()%>"  height="50px" width="50px"/>
                                        </div>
                                    </div> 

                                </td> 

                            </tr>           

                            <%}
                            %>




                        </table>
                    </form>
                </center>
            </div>
                           
            <div id="lop" style="float: right;width: 60%;">

               

                <div class="input-group input-number-group" style="width:50%;float: right;border-left-style: ridge">
                    <br/><br/>
                    <table>
                        <tr>
                            <td><label>Nom</label></td>
                            <td> <div class="w3-card-4" style="width:200px">
                                    <div class="w3-container w3-center" id="nomp">

                                    </div>
                                </div> 
                            </td>
                        </tr>
                        <tr>
                            <td><label>Prix</label></td>
                            <td> <div class="w3-card-4" style="width:200px">
                                    <div class="w3-container w3-center" id="prixp">

                                    </div>
                                </div> 
                            </td>
                        </tr>
                        <tr>
                            <td><label>Description</label></td>
                            <td> <div class="w3-card-4" style="width:200px">
                                    <div class="w3-container w3-center" id="description">

                                    </div>
                                </div> 
                            </td>
                        </tr>
                    </table>
                    <br/>
                    
                    <table>
                        <tr>
                            <td> 
                                <div class="w3-card-4" style="width:100px">
                                    <div class="w3-container w3-center" id="plus" >
                                         <span id="plus">+</span>
                                    </div>
                                </div> 
                            </td>
                             <td>
                                <div class="w3-card-4" style="width:100px">
                                    <div class="w3-container w3-center" id="qteT">
                                        <input class="input-numbe" type="number" value="1" min="1" max="1000" style="width:80px">
                                        <input id="idp" type="hidden"/>
                                     </div>
                                </div> 

                            </td>
                            <td>
                                
                             <div class="w3-card-4" style="width:100px">
                                    <div class="w3-container w3-center" id="moins" >
                                          <span id="moins">-</span>
                                    </div>
                                </div> 
                                
                            </td>

                        </tr>
                        <tr>
                            <td>   </td>
                            <td>
                                
                             <div class="w3-card-4" style="width:100px">
                                    <div class="w3-container w3-center" id="Panier" >
                                        <span id="panier"> <a ><i class='fas fa-cart-plus' style='font-size:20px'></i></a></span>
                                    </div>
                                </div> 
                                
                            </td>

                        </tr>
                    </table>



                </div>

            </div>

        </div>

    </body>
</html>



    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>

    <script>

        $(document).ready(function () {

            $(".koko").click(function () {

                var id = $(this).attr('id');
                var nom = $(this).val();

                $.post("./serv", {name: nom}, function (data) {
                    var donnees = data.split("*");
                    $('#nomp').html(donnees[2]);
                    $('#prixp').html(donnees[4]);
                    $('#description').html(donnees[1]);
                    $("#lop1").attr("src", donnees[3]);
                    $("#idp").val(donnees[2]);




                });

            });
            
            $("#plus").click(function() {
          var input = $(this).parents('.input-number-group').find('.input-numbe');      
          var val = parseInt(input.val(), 10);
            input.val(val + 1);
                
        });
            $("#moins").click(function() {
          var input = $(this).parents('.input-number-group').find('.input-numbe');      
          var val = parseInt(input.val(), 10);
          
            if(val<=1){
                  input.val(1);
            }else
            {
                  input.val(val - 1);
            }
                
        });
        
        //panier
        $("#panier").click(function() {
            var qte= $(".input-numbe").val();
            var nomprod= $("#idp").val();
      alert("ajout reussi");
      $.post("./superserv",{quantite:qte,nomprod:nomprod},function(data){
          
      });
      
        });
        
});



    </script>

</html>

